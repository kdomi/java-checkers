
 Java checkers game
=============================================

About
-----------------------------------------------
This software was written as part of the first and third assignment in "Agile
 Object-Oriented Software Developement". The course is taught by the
 Technical University of Denmark.

The course is an introduction to the programming language Java and all
 it's aspects, I made this code after one month of learning the language.


Start
-----------------------------------------------
In the terminal run "javac checkers" so it compiles the program. The game
 is set in the terminal.




Licensing
-------------
Copyright 2019 Dominik Kovács

This is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with it.  If not, see https://www.gnu.org/licenses/.

