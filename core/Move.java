// Move class, checkers
package core;
import java.util.Scanner; // Importing for scanner

public class Move{
    private int player, xPiece, yPiece, xPosition, yPosition;
    
    public void setVariables(int player_new, int xPiece_new, int yPiece_new){
        player = player_new;
        xPiece = xPiece_new;
        yPiece = yPiece_new;
    }
    public void askPlayerMove(Scanner s, Board table){
        System.out.println("Coordinate of new position");   
        xPosition = checkers.inputHandler("Enter X: ",s);
        yPosition = checkers.inputHandler("Enter Y: ",s);  
              
        // Judge selection
        if(!judgeMove(table.matrix,xPiece,yPiece,xPosition,yPosition, player)){
            askPlayerMove(s, table); // ask again if failed
        }
    }
    
    // To handle choice of move
    public boolean judgeMove(int matrix [][],int xPiece,int yPiece,int xPosition,int yPosition,int player) {
        if ((xPosition > 7 || xPosition < 0) || (yPosition > 7 || yPosition < 0)){
            checkers.gameMessage("Invalid move, select a coordinate on the board!");
            return false;
        }else if(matrix[yPosition][xPosition] != 0){
            checkers.gameMessage("Invalid move, you cannot move to a field which is occupied!");
            return false;
        }else if((Math.abs(xPosition-xPiece) == 2) && (Math.abs(yPosition-yPiece) == 2)){
            // Identifying the location of the cross piece
            int xEnemy = xPiece > xPosition ? xPosition + 1 : xPosition - 1; 
            int yEnemy = yPiece > yPosition ? yPosition + 1 : yPosition - 1;
            int target = matrix[yEnemy][xEnemy];
            // Finding out whether it's friendly or enemy
            if(target == player || target == 0){
                checkers.gameMessage(target == player ? "You Cannot jump over own pieces!" : "You can only jump over enemies!");
                return false;
            }else{
                matrix[yPiece][xPiece] = 0;
                matrix[yPosition][xPosition] = player;
                matrix[yEnemy][xEnemy] = 0;
                checkers.gameMessage("Piece moved!");
                return true;
            }
        }else if((Math.abs(xPosition-xPiece) != 1) || (Math.abs(yPosition-yPiece) !=1)){
            checkers.gameMessage("Invalid move, you can only move diagonally!");
            return false;

        }else{
            matrix[yPiece][xPiece] = 0;
            matrix[yPosition][xPosition] = player;
            checkers.gameMessage("Piece moved!");
            return true;
        }
    }
}
