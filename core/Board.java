// Board class, checkers
package core;
public class Board{
    public int[][] matrix = {
      { 0, 1, 0, 1, 0, 1, 0, 1},
      { 1, 0, 1, 0, 1, 0, 1, 0},
      { 0, 1, 0, 1, 0, 1, 0, 1},
      { 0, 0, 0, 0, 0, 0, 0, 0},
      { 0, 0, 0, 0, 0, 0, 0, 0},
      { 2, 0, 2, 0, 2, 0, 2, 0},
      { 0, 2, 0, 2, 0, 2, 0, 2},
      { 2, 0, 2, 0, 2, 0, 2, 0}
    };
    // Welcome message
    void Board(){  
        System.out.println("\n--------------WELCOME TO CHECKERS--------------");
        System.out.println("  -------------- MADE BY KDOMI --------------");
        System.out.println("         -------  VERSION 0.9  -------\n");
        System.out.println(" Info: - Type any letter if you want to cancel piece or movement selection.");
        System.out.println("       - You win if all the enemy pieces are dead.");
        System.out.println("       - Type 'Exit' to shut down the program.\n");
    }
    // To display the board
    public void printBoard() {
        System.out.println("   0 1 2 3 4 5 6 7   <-  X axis");
        System.out.println("  +----------------+");
        for(int i=0; i<8; i++){
            System.out.print(i+" |");
            for(int j=0; j<8; j++){
                if(matrix[i][j] == 0){
                    System.out.print("  ");
                }else {
                System.out.print(matrix[i][j]+" ");
                }
            }
            System.out.println("|");    
        }
        System.out.println("  +----------------+");
        System.out.println("   0 1 2 3 4 5 6 7 ");
    }
    
}

