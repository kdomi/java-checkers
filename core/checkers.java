//
//  Assignment 3: Checkers
//  Dominik Kovacs s174383
//
package core;
import java.util.Scanner; // Importing for scanner


public class checkers{
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        Board table = new Board();
        Selection sel = new Selection();
        Move mov = new Move();
        while(true){
             table.printBoard();
             sel.askPlayerPiece(s,table, mov);
             mov.askPlayerMove(s,table);
        }
    }
    // To print error mesages
    public static void gameMessage(String s){
        System.out.println("\n-----------------------------------\n"+s);
        System.out.println("-----------------------------------");
    }
    // To handle user input
    public static int inputHandler(String message,Scanner s){
        System.out.print(message);
        String input = s.nextLine();
        if(input.equals("Exit")){
            System.out.println("\nThanks for playing.\nExiting Program...");
            System.exit(0);
            return -1;
        }
        try{
            return Integer.parseInt(input);
        }catch(NumberFormatException e){
                System.out.println("\n-----------------------------------");
                System.out.println("Type only numbers!");
                System.out.println("-----------------------------------\n");
                return inputHandler(message,s);
        }
    }    
}
