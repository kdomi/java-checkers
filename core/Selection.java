// Selection class, checkers
package core;
import java.util.Scanner; // Importing for scanner

public class Selection{
    private int player = 1; // current player
    private int xPiece, yPiece; // Coordinates of chosen piece
    
    
    private void switchPlayer(){
        player = player == 1 ? 2 : 1;
    }
    
    public void askPlayerPiece(Scanner s, Board table, Move mov){
        System.out.println("Turn of player no. "+player);
        
        System.out.println("Coordinate of the piece to move");   
        xPiece = checkers.inputHandler("Enter X: ",s);
        yPiece = checkers.inputHandler("Enter Y: ",s);  
              
        // Judge selection
        if(!judgeSelection(table.matrix,xPiece,yPiece,player)){
            askPlayerPiece(s, table, mov); // ask again if failed
        }
        mov.setVariables(player, xPiece, yPiece); // Send the final vars to move
        switchPlayer(); // Switch player for next turn
    }
    
    // To handle choice of piece
    public boolean judgeSelection(int matrix [][],int xPiece,int yPiece,int player) {
        if ((xPiece > 7 || xPiece < 0) || (yPiece > 7 || yPiece < 0)){
            checkers.gameMessage("Invalid selection, select a coordinate on the board!");
            return false;
        }else if (matrix[yPiece][xPiece] == 1){
            if(player == 2){
                checkers.gameMessage("You cannot select the piece of player 1!");
                return false;
            } else{return true;}
        } else if(matrix[yPiece][xPiece] == 2){
            if(player == 1){
                checkers.gameMessage("You cannot select the piece of player 2!");
                return false;
            } else{return true;}
        } else{
            checkers.gameMessage("Please select a proper piece!");
            return false;
        }
    }
}
