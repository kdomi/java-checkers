package test;

import static org.junit.Assert.*;
import org.junit.Test;
import core.Board;
import core.Selection;
import core.Move;

public class checkersTest {
	Board table = new Board();
	Selection sel = new Selection();
	Move mov = new Move();
	int[][] matrix = table.matrix;
	
	@Test
	public void testJudgeSelection() {
		assertTrue("Should be legal to pick these", sel.judgeSelection(matrix, 1, 0, 1));
		assertFalse("Should be illegal to pick these", sel.judgeSelection(matrix, 0, 0, 1));
		assertTrue("Should be legal to pick these", sel.judgeSelection(matrix, 0, 5, 2));
		
	
	}

	@Test
	public void testJudgeMove() {
		assertTrue("Should be legal to move these", mov.judgeMove(matrix, 1, 2, 0, 3, 1));
		assertFalse("Should be illegal to move these", mov.judgeMove(matrix, 1, 2, 1, 3, 1));
		assertTrue("Should be legal to move these", mov.judgeMove(matrix, 0, 5, 1, 4, 2));
		
	}

}
